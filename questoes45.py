import math


def get_log10(number):
    return math.log10(number)


def get_sum_list(list):
    split_list = list.split(",")
    new_list = []
    for i in split_list:
        new_list.append(int(i))
    return sum(new_list)
