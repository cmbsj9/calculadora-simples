def listar_ordenar_decrescente(lista):
    try:
        return sorted(lista, reverse=True)
    except TypeError:
        return "Erro: Lista contém tipos de dados não comparáveis."


def potencia(base, expoente):
    try:
        return float(base) ** float(expoente)
    except (ValueError, TypeError):
        return "Erro: Deve ser inserido somente números."
    


