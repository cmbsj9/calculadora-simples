from questoes16 import subtracao, multiplicacao
from operacao_pares import listar_ordenar_decrescente, potencia
from questoes45 import get_log10, get_sum_list


def menu():
    while True:
        print("Selecione a opção:")
        print("1. Subtração")
        print("2. Ordenar uma lista por ordem decrescente")
        print("3. Potência")
        print("4. Logaritmo com base 10")
        print("5. Somar todos os elementos de uma lista")
        print("6. Multiplicação")
        print("7. Sair")

        opcao = input("Opção: ")

        if opcao == "1":
            subtracao()
        elif opcao == "2":
            lista = input("Digite uma lista de números separados por espaço: ")
            lista = [float(i) for i in lista.split()]
            print("Lista ordenada decrescente:", listar_ordenar_decrescente(lista))
        elif opcao == "3":
            base = input("Digite a base: ")
            expoente = input("Digite o expoente: ")
            print("Potência:", potencia(base, expoente))
        elif opcao == "4":
            valor = input("Digite a valor: ")
            log = get_log10(valor)
            print("Logaritmo base 10:", log)
        elif opcao == "7":
            print("Saindo...")
            break
        else:
            print("Opção inválida. Por favor, escolha uma opção válida.")


if __name__ == '__main__':
    menu()
